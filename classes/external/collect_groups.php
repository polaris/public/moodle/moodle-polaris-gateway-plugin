<?php
namespace local_polaris_gateway\external;

use context_system;
use external_function_parameters;
use external_multiple_structure;
use external_single_structure;
use external_value;

class collect_groups extends \external_api {

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function execute_parameters() {
        return new external_function_parameters([
            'user_email' =>  new external_value(PARAM_TEXT, 'email of the user')
        ]);
    }

    public static function execute_returns() {
        return new external_multiple_structure(
            new external_single_structure([
                'key' => new external_value(PARAM_TEXT, 'id of course'),
                'user_friendly_name' => new external_value(PARAM_TEXT, 'multilang compatible name, course unique'),
                'description' => new external_value(PARAM_TEXT, 'group description text'),
            ])
        );
    }

    public static function execute($user_email) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::execute_parameters(), ['user_email' => $user_email]);

        // now security checks
        $context = context_system::instance();
        self::validate_context($context);
        require_capability('local/polaris_gateway:access_api', $context);

        $result = [];

        $user = $DB->get_record('user', ['email' => $params["user_email"]]);
        $courses = enrol_get_users_courses($user->id, true);

        foreach($courses as $course)
        {
            $singleCourse = [];
            $singleCourse["key"] = $course->id;
            $singleCourse["user_friendly_name"] = $course->shortname;
            $singleCourse["description"] = $course->name;
            $result[] = $singleCourse;
        }

    
        return $result;
    }
}
