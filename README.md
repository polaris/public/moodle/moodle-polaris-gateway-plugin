# Polaris Gateway #

The plugin is a gateway for the polaris infrastructure, espaclliy the rights engine. 
We provide an external web service to access the required data.

Finally, you should open the following page and see the course structure the user with the email USER_EMAIL.
https://HOST/webservice/rest/server.php?wstoken={GENERATED_TOKEN}&wsfunction=local_polaris_gateway_collect_groups&moodlewsrestformat=json&user_email={USER_EMAIL}

## Installing via uploaded ZIP file ##

1. Log in to your Moodle site as an admin and go to _Site administration >
   Plugins > Install plugins_.
2. Upload the ZIP file with the plugin code. You should only be prompted to add
   extra details if your plugin type is not automatically detected.
3. Check the plugin validation report and finish the installation.

## Installing manually ##

The plugin can be also installed by putting the contents of this directory to

    {your/moodle/dirroot}/local/polaris_gateway

Afterwards, log in to your Moodle site as an admin and go to _Site administration >
Notifications_ to complete the installation.

Alternatively, you can run

    $ php admin/cli/upgrade.php

to complete the installation from the command line.

## Configuration ##

1. Activiate the web services in moodle
2. Create a service user
3. Allow the service user to use the Polaris Gateway service
4. Generate a token and add the url to the provider schema

## License ##

2023 Digital Learning GmbH <support@digitallearning.gmbh>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <https://www.gnu.org/licenses/>.
