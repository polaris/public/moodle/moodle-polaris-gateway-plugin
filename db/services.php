<?php 

$functions = [
    // The name of your web service function, as discussed above.
    'local_polaris_gateway_collect_groups' => [
        // The name of the namespaced class that the function is located in.
        'classname'   => 'local_polaris_gateway\external\collect_groups',
        'classpath' => 'local/polaris_gateway/classes/external/collect_groups.php',

        // A brief, human-readable, description of the web service function.
        'description' => 'Collects information about courses members to show them in polaris',

        // Options include read, and write.
        'type'        => 'read',

        // Whether the service is available for use in AJAX calls from the web.
        'ajax'        => true,

        // An optional list of services where the function will be included.
        'services' => [
        ]
    ],
];

$services = [
    'Polaris Gateway' => [
        'functions' => [
            'local_polaris_gateway_collect_groups',
        ],
        'restrictedusers' => 1,
        'enabled' => 1,
        'shortname' => 'polaris_gateway',
    ]
];