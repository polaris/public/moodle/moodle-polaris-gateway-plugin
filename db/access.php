<?php


$capabilities = [
    'local/polaris_gateway:access_api' => [
        'riskbitmask' => RISK_PERSONAL ,
        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
    ],
];